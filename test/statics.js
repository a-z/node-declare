var assert = require('assert'),
    declare = require('../declare');

describe('declare::statics', function() {
    it('should be parsed and provided as static members.', function() {
        var cls = declare({
            statics: {
                foo: function() {
                    return this.bar;
                },

                bar: 'ok'
            }
        });

        assert(typeof cls.foo === 'function');
        assert(typeof cls.bar === 'string');

        assert(cls.foo() === 'ok');
    });
});
