var assert = require('assert'),
    declare = require('../declare');

describe('declare::api', function() {
    it('should create a new class', function() {
        var cls = declare({});

        assert.equal(typeof cls, 'function');
    });

    it('should provide #super() method', function() {
        var cls = declare({});

        assert.equal(typeof (new cls).super, 'function');
    });

    it('should inject methods', function() {
        var cls = declare({
            foo: function() {
                return 'check';
            },

            bar: function() {
                return this.foo();
            }
        });

        var instance = new cls;

        assert.equal(typeof instance.foo, 'function');
        assert.equal(typeof instance.bar, 'function');

        assert.equal('check', instance.foo());
        assert.equal('check', instance.bar());
    });

    it('should create properties/methods that are fully writable by default', function() {
        var cls = declare({
            foo: 1
        });

        var instance = new cls;

        assert('foo' in instance, 'Error finding property foo.');

        instance.foo = 2;
        assert.equal(instance.foo, 2, 'Error changing the value.');

        instance.foo = 'check';
        assert.equal(instance.foo, 'check', 'Error changing the type of the value.');

        delete instance.foo;

        // @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete#Examples
        // will reset the value, since inherited properties from prototype cannot be deleted on the object.
        assert.equal(instance.foo, 1);
    });
});
