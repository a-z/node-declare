var assert = require('assert'),
    declare = require('../declare');

describe('declare::inheritance', function() {
    function parent() {};
    function ctor() {};

    it('should do propert inheritance', function() {
        var cls = declare({
            extend: parent,
            constructor: ctor
        });

        assert.equal(cls.super_, parent);
        assert((new cls) instanceof parent, 'declared class is not instance of its parent.');
        assert((new cls) instanceof ctor, 'declared class is not instance of its constructor.');
    });

    it('should do proper inheritance with anonymous constructor', function() {
        var cls = declare({
            extend: parent,
            constructor: function() {}
        });

        assert.equal(cls.super_, parent);
        assert((new cls) instanceof parent, 'declared class is not instance of its parent.');
    });

    it('should do proper inheritance with no constructor given', function() {
        var cls = declare({
            extend: parent
        });

        assert.equal(cls.super_, parent);
        assert((new cls) instanceof parent, 'declared class is not instance of its parent.');
    });

    it('should execute parent constructor, if none is given.', function() {
        var parentClass = declare({
            value: null,
            args: null,

            constructor: function ParentClass(param) {
                this.value = param;
                this.args = Array.prototype.slice.call(arguments);
            }
        }), cls = declare({
            extend: parentClass
        });

        var instance = new cls('ok', 123, false);

        assert(instance.value === 'ok');
        assert(instance.args.length === 3);
        assert(instance.args[0] === 'ok');
        assert(instance.args[1] === 123);
        assert(instance.args[2] === false);
    });

    it('should work well with a deep level of inheritance (1000).', function() {
        var ctx = declare({
                foo: function() { return 'ok'; }
            }),_stack = [ctx],
        LEVEL = 1000,
        i = LEVEL;

        while (--i) {
            ctx = declare({
                extend: _stack[_stack.length - 1]
            });

            _stack.push(ctx);
        }

        assert.doesNotThrow(function() {
            new ctx;
        });

        assert((new ctx).foo() === 'ok');

        while (ctx.super_) {
            ctx = ctx.super_;
            ++i
        }

        assert(i === LEVEL)
    });
});
