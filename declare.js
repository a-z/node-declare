function baseClass() {};

module.exports = function declare(definition) {
    var ctor,
        parent = null, props, propertyName, propertyDefinition;

    if (definition.hasOwnProperty('constructor')) {
        ctor = definition.constructor
    } else {
        ctor = function anonymousCtor() {
            parent.prototype.constructor.apply(this, arguments)
        };
    }

    if (typeof definition.extend === 'function') {
        parent = definition.extend;
    } else {
        parent = baseClass;
    }

    ctor.super_ = parent;

    props = {
        constructor: {
            value: ctor
        },
        super: {
            value: function(fname) {
                switch(arguments.length) {
                    case 1:
                        return parent.prototype[fname].call(this);
                    case 2:
                        return parent.prototype[fname].call(this, arguments[1]);
                    case 3:
                        return parent.prototype[fname].call(this, arguments[1], arguments[2]);
                    case 4:
                        return parent.prototype[fname].call(this, arguments[1], arguments[2], arguments[3]);
                    case 5:
                        return parent.prototype[fname].call(this, arguments[1], arguments[2], arguments[3], arguments[4]);
                    default:
                        return parent.prototype[fname].apply(this, Array.prototype.slice.call(arguments, 1));
                }
            }
        }
    };

    for (propertyName in definition) {
        switch(propertyName) {
            case 'constructor':
            case 'extend':
                break;
            case 'statics':
                var staticsDefinitions = definition.statics;

                for (var staticProperty in staticsDefinitions) {
                    ctor[staticProperty] = staticsDefinitions[staticProperty];
                }

                break;
            default:
                propertyDefinition = definition[propertyName];

                if (!propertyDefinition || typeof propertyDefinition !== 'object' || !('value' in propertyDefinition)) {
                    propertyDefinition = {
                        value: propertyDefinition,
                        writable: true,
                        enumerable: true
                    };
                }

                props[propertyName] = propertyDefinition;
                break;
        }
    }

    ctor.prototype = Object.create(parent.prototype, props);

    return ctor;
};

