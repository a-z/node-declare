[![NPM](https://nodei.co/npm/decl.png?downloads=true&downloadRank=true)](https://nodei.co/npm/decl/)
---
[![build status](https://ci.gitlab.com/projects/3641/status.png?ref=master)](https://ci.gitlab.com/projects/3641?ref=master)

## Install ##

`$ npm install decl`

## How to use ##

```js
var cls = require('decl')({
    extend: ParentCls,

    constructor: function MyClass(arg1, arg2) {
        this.super('constructor', arg1, arg2);
    },

    method1: function() {
        //
    }
});
```

### Class Statics ###

For defining static members on a class, simply pass a hash to the **statics** configuration key.
These properties can then be accessed on class level. The *this* keyword resolves to the class within static members.

```js
var cls = require('decl')({
    statics: function() {
        NAME: 'my/class',

        getName: function() {
            return this.NAME;
        }
    }
});

cls.getName() === 'my/class';
typeof (new cls).NAME === 'undefined';
```

## ToDos

* Improve the documentation
* Add support for mixins/traits
* Add support for inheritence of static members (static super?)
